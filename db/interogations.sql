use mt_dev;

select p.name
from plays p join schedules s on s.play_id = p.id
where s.theatre_id = 1;

select id, (select count(distinct s.play_id) from schedules s where s.theatre_id = t.id) cnt 
from theatres t;

select id, ifnull((select sum(ticket_price) from schedules where play_id = p.id), 0) total_sum
from plays p;
