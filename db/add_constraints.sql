use mt_dev;

alter table schedules
drop constraint FK_theatre_id;

alter table schedules
add constraint FK_theatre_id foreign key (theatre_id) references theatres(id) on delete cascade;

alter table schedules
drop constraint FK_play_id;

alter table schedules
add constraint FK_play_id foreign key (play_id) references plays(id) on delete cascade;
