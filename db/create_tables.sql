use mt_dev;

create table theatres (
  id int PRIMARY KEY,
  name varchar(255),
  city varchar(255),
  director_name varchar(255)
);

drop table schedules;

create table schedules (
  id int primary key,
  theatre_id int,
  play_id int,
  date_time date,
  ticket_price int
);

create table plays (
  id int primary key,
  name varchar(255),
  writer_name varchar(255),
  director_name varchar(255),
  description varchar(255)
);
