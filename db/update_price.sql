use mt_dev

select * from schedules;

select @minp:=min(ticket_price)
from schedules;

update schedules
set ticket_price = ticket_price + id
where ticket_price = @minp;

select * from schedules;
