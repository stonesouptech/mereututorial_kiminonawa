use mt_dev;

insert into theatres
values (1, 'Augustus', 'Rome', 'director_name_1');

insert into theatres
values (2, 'Septembrius', 'Cluj', 'director_name_2');

insert into plays
values (1, 'play_name_1', 'writer_name_1', 'director_name_1', 'description1');

insert into plays
values (2, 'play_name_2', 'writer_name_2', 'director_name_2', 'description2');

insert into plays
values (3, 'play_name_3', 'writer_name_3', 'director_name_3', 'description3');

insert into plays
values (4, 'play_name_4', 'writer_name_4', 'director_name_4', 'description4');

insert into schedules
values (1, 1, 2, str_to_date('20 09 2020', '%d %m %Y'), 1000);

insert into schedules
values (2, 2, 3, str_to_date('27 09 2021', '%d %m %Y'), 2700);

insert into schedules
values (3, 2, 1, str_to_date('20 08 2020', '%d %m %Y'), 8);

insert into theatres
values (3, 'Januarius', 'London', 'director_name_3');

select * from theatres;
select * from plays;
select * from schedules;
